from django import forms
from .models import Jadwal_Model
# Create your models here.

'''class Jadwal_Form(forms.Form):
    nama_kegiatan = forms.CharField(label='Nama Kegiatan', required=True)
	#hari, tanggal, jam, 
    def clean_tanggal_dan_waktu(self):
        data = self.cleaned_data['tanggal_dan_waktu']
            # Check if a date is not in the past. 
        if data < datetime.date.today():
            raise ValidationError(_('Invalid date - renewal in past'))
            # Remember to always return the cleaned data.
        return data'''

class Jadwal_Form(forms.ModelForm):
    tanggal_kegiatan= forms.DateField(required=True,widget=forms.DateInput(attrs={'type':'date'}))
    waktu_kegiatan = forms.TimeField(required=True,widget=forms.TimeInput(attrs={'type':'time'}))
    
    class Meta:
        model = Jadwal_Model
        fields = ('nama_kegiatan', 'tanggal_kegiatan','waktu_kegiatan', 'tempat_kegiatan','kategori_kegiatan',)