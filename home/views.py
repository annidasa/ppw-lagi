from django.shortcuts import render
from django.http import HttpResponseRedirect
from home.forms import Jadwal_Form
from django.shortcuts import get_object_or_404
from django.urls import reverse
from .forms import Jadwal_Form
from .models import Jadwal_Model


# Create your views here.
def index(request):
	return render(request, "latjuga.html", {})

def challenge(request):
	return render(request,"challenge.html",{})

def projects(request):
	return render(request,"latjuga2.html",{})

def list_kegiatan(request):
	response = {}
	listkegiatan = Jadwal_Model.objects.all()
	response['listkegiatan'] = listkegiatan
	return render(request, "listkegiatan.html", response)
	
def jadwal_form(request):
	if (request.method == 'POST'):
		# Create a form instance and populate it with data from the request (binding):
		form_jadwal = Jadwal_Form(request.POST)
		# Check if the form is valid:
		if form_jadwal.is_valid():
			# process the data in form.cleaned_data as required (here we just write it to the model due_back field)
			form_jadwal.save()
			# redirect to a new URL
			return HttpResponseRedirect(reverse('kegiatan-list'))
    # If this is a GET (or any other method) create the default form.
	else:
		form = Jadwal_Form
		return render(request, 'formkegiatan.html', {'form': form})


def hapus_jadwal(request):
	if (request.method == 'POST'):
		Jadwal_Model.objects.all().delete()
	return HttpResponseRedirect(reverse('kegiatan-list'))