from django.db import models
# Create your models here.

class Jadwal_Model(models.Model):
	nama_kegiatan = models.CharField(max_length=40)
	tanggal_kegiatan= models.DateField()
	waktu_kegiatan = models.TimeField()
	tempat_kegiatan= models.CharField(max_length=40)
	Options = [
        ('kegiatan kuliah', 'kegiatan kuliah'),
        ('acara keluarga', 'acara keluarga'),
        ('rapat', 'rapat'),
        ('event penting', 'event penting'),
      ]
	kategori_kegiatan=models.CharField(max_length=40,choices=Options)