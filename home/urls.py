from django.urls import path
import home
from .views import *

urlpatterns = [
    path('', index, name='home-index'),
    path('challenge/', challenge, name='challenge'),
    path('projects/', projects, name='projects'),
    path('kegiatan/',list_kegiatan,name='kegiatan-list'),
    path('kegiatan/add-new', jadwal_form, name='kegiatan'),
    path('kegiatan/del', hapus_jadwal, name='kegiatan-del'),
]